package com.example.demo.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@Column(name= "Employee_Name")
	private String empName;
	@Column(name= "Employee_DOB")
	private int empDOB;
	@Column(name= "Employee_Age")
	private String empAge;
	@Column(name= "Employee_Salary")
	private int empSalary;

}
