package com.example.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DAO.EmployeeDAO;
import com.example.demo.Model.Employee;

@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeDAO empdao;
	
	@CrossOrigin(origins = "http://localhost:8080")
	@GetMapping("/all")
	public @ResponseBody Iterable<Employee> getallemployees()
	{
		return empdao.findAll();
	}
	@GetMapping("/test")
	public String gettest(){
		return "working";
	}
}
